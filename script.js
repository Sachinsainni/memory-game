const gameContainer = document.getElementById("game");
const replay = document.getElementById('replay');
const results = document.getElementById('result');
const playButton = document.getElementById('play-button');
const mainPage = document.getElementById('first');
const playPage = document.getElementById('play-page');
const topScore = document.getElementById('top-score')
const LocalHighestScore = window.localStorage.getItem('highestScore');
const header = document.getElementById('head')

if(LocalHighestScore===null){
  topScore.innerHTML = `Highest Score : 0`
}else{
  topScore.innerHTML = `Highest Score : ${LocalHighestScore}`
}

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "yellow",
  "violet"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more

let GIFS = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif"
];

function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let requriedGIFS = [...GIFS, ...GIFS]
let shuffledColors = shuffle(requriedGIFS);
let shuffledGIFS = shuffle(GIFS);
let requiredColors = [...shuffledColors, "#fff", ...shuffledColors]

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray, gifArray) {
  for (let index = 0; index < gifArray.length; index++) {
    // const element = gifArray[index];
    // console.log(element);
    const newImg = document.createElement("div")
    const image = document.createElement("img")
    image.src = gifArray[index];
    image.classList.add("imageClass");
    // newImg.style.background = element;
    newImg.addEventListener("click", handleCardClick);
    gameContainer.append(newImg);
    newImg.append(image);
  }
}

// TODO: Implement this function!
let hasCard = false;
let firstCard;
let clickCount = 0;
let attempt = 0 ;

function handleCardClick(event) {
  attempt +=1 ;
  if (firstCard === undefined) {
    firstCard = event.target;
    event.target.querySelector("img").style.display = 'block'
    event.target.removeEventListener('click', handleCardClick);

  } else if (firstCard.querySelector('img').src === event.target.querySelector('img').src) {
    event.target.querySelector("img").style.display = 'block'
    clickCount += 1;
    event.target.removeEventListener('click', handleCardClick);
    firstCard.removeEventListener('click', handleCardClick);
    firstCard = undefined;
  } else {
    event.target.querySelector("img").style.display = 'block';
    document.querySelector('#game').style.pointerEvents = 'none';
    setTimeout(() => {
      firstCard.querySelector("img").style.display = 'none';
      event.target.querySelector("img").style.display = 'none';
      firstCard.addEventListener('click', handleCardClick);
      firstCard = undefined;
      document.querySelector('#game').style.pointerEvents = 'auto';
    }, 1000);
  }
  console.log(clickCount);

  if(clickCount == 12){
    results.innerHTML = `Congratulation You Won the match after ${attempt} clicks`;
    if(attempt<LocalHighestScore){
      localStorage.setItem('highestScore',attempt);
    }
  }
}
playButton.addEventListener('click',()=>{
  mainPage.style.display= 'none';
  playPage.style.display = 'block'
  header.style.display = 'none'
})
// when the DOM loads
createDivsForColors(requiredColors, requriedGIFS);
